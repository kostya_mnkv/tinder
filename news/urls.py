from django.conf.urls import url
from news.views import ArticleListView
urlpatterns = [
    url(r'^$', ArticleListView.as_view(), name="all_articles"),
]
from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=120)
    slug = models.SlugField()

    def __str__(self):
        return str(self.name)

def generate_filename(instance, filename):
    filename = instance.slug+'.jpg'
    return "{0}".format(filename)

class Article(models.Model):
    category = models.ForeignKey(Category)
    title = models.CharField(max_length=120)
    slug = models.SlugField()
    image = models.ImageField(upload_to=generate_filename)
    content = models.TextField()
    likes = models.PositiveIntegerField(default=0)
    dislikes = models.PositiveIntegerField(default=0)

    def __str__(self):
        return "Статья {0} из категории {1}".format(self.title, self.category.name)





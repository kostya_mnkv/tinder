from django.shortcuts import render
from news.models import Category, Article
from django.views.generic.list import ListView
# Create your views here.

class ArticleListView(ListView):
    model = Article
    template_name = "news/index.html"


    def get_context_data(self, *args, **kwargs):
        context = super(ArticleListView, self).get_context_data(*args, **kwargs)
        context["articles"] = self.model.objects.all()
        return context